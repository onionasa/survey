from django import forms
from django.forms import ModelForm
from survey_base.models import Choice, Question

__author__ = 'orionasa'

class ChoiceForm(ModelForm):
    class Meta:
        model=Choice
        fields=('question','choice_text')
        question = forms.ModelChoiceField(queryset=Question.objects.all())
