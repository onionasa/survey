__author__ = 'orionasa'

from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$',views.IndexView.as_view(),name='index'),
    url(r'^(?P<pk>[0-9]+)/detail/$',views.DetailView.as_view(),name='detail'),
    url(r'^(?P<pk>[0-9]+)/result/$',views.ResultsView.as_view(),name='result'),
    url(r'^(?P<question_id>[0-9]+)/$',views.VoteView.as_view(),name='vote'),
    url(r'addquestion/$', views.QuestionAdd.as_view(template_name='question_form.html'), name='question_add'),
    url(r'addchoice/$', views.ChoiceAdd.as_view(template_name='choice_form.html'), name='choice_add'),
]